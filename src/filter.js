function filter(arrayToFilter, predicate) {
  if (arguments.length === 0) {
    throw new Error("Please provide an array and predicate function.");
  }
  if (arguments.length === 1) {
    throw new Error("Expected a predicate as the second argument");
  }
  if (!(arrayToFilter instanceof Array)) {
    throw new Error("Expected an array as the first argument");
  }
  if (!(predicate instanceof Function)) {
    throw new Error("Expected the predicate to be a function");
  }

  const filteredArray = []
  
  arrayToFilter.forEach(item => {
    const shouldKeepItem = predicate(item)
    if(shouldKeepItem) {
      filteredArray.push(item)
    }
  });
  return filteredArray
}

module.exports = filter;
